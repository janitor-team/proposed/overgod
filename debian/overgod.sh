#!/bin/sh

MASTER="/usr/share/games/overgod/default.cfg"
CONFIG="${HOME}/.overgod.cfg"

if [ ! -f "${CONFIG}" ]; then
    cp "${MASTER}" "${CONFIG}" >/dev/null 2>&1
fi

exec /usr/lib/games/overgod/overgod
