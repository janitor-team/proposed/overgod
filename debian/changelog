overgod (1.0-6) unstable; urgency=medium

  * Team upload.
  * Fix FTCBFS: Let dpkg's buildtools.mk supply $(CC). (Closes: #916491)
    Thanks to Helmut Grohne for the fix
  * Use debhelper-compat 11
  * Upgrade to Standards Version 4.4.1 (No changes required)
  * Use salsa repositories in Vcs-* fields
  * Fix spelling error in patch 030_security_build_warns.patch
    Desription/Description

 -- Andreas Rönnquist <gusnan@debian.org>  Wed, 16 Oct 2019 16:38:44 +0200

overgod (1.0-5) unstable; urgency=medium

  * Team upload.
  * Switch to compat level 10.
  * wrap-and-sort -sa.
  * Declare compliance with Debian Policy 4.0.0.
  * Use canonical Vcs-URI.
  * Drop deprecated overgod.menu file.
  * overgod.desktop: Add a comment in German.
  * Use the C locale for listing the source files and make the build
    reproducible. Thanks to Reiner Herrmann for the report and patch.
    (Closes: #827114)
  * Fix debian/watch file.
  * d/control: Add homepage field.
  * Fix Lintian warning spelling-error-in-manpage.

 -- Markus Koschany <apo@debian.org>  Wed, 19 Jul 2017 00:00:56 +0200

overgod (1.0-4.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop patch 060_gcc5.patch.
    This only works with gcc5 and leads to FTBFS with gcc4.9.
  * Add -fgnu89-inline to CFLAGS in debian/rules instead.

 -- gregor herrmann <gregoa@debian.org>  Fri, 24 Jul 2015 00:58:52 +0200

overgod (1.0-4.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix "ftbfs with GCC-5": add patch from Nicholas Luedtke:
    add "extern" to inline functions.
    (Closes: #778051)

 -- gregor herrmann <gregoa@debian.org>  Sat, 18 Jul 2015 21:31:19 +0200

overgod (1.0-4) unstable; urgency=low

  * Team upload.
  * Add VCS fields in debian/control.
  * Dpkg does not replace symlinks with directories and vice versa, therefore
    maintainer scripts need to be used. Add overgod.preinst script.
    (Closes: #720153)
  * Add keywords to desktop file.
  * Do not refer to symlink license in debian/copyright.
  * Install readme.txt with overgod.docs.

 -- Markus Koschany <apo@gambaru.de>  Tue, 17 Sep 2013 20:20:04 +0200

overgod (1.0-3) unstable; urgency=low

  * Adopt package for the Debian Games Team. (Closes: #711563).
    + Set maintainer to Debian Games Team.
    + Add myself to uploaders.
  * Move to source format 3.0 (quilt).
    + Drop quilt from build-depends.
  * Add watch file.
  * Add ${misc:Depends} for packages.
  * Add desktop file and put icon in correct directory. (Closes: #689635).
    + Fix Debian menu file as well.
  * Add patch to fix array overrun. (Closes: #716604).
  * Add patch to fix speaker test. (Closes: #454806).
    + Thanks to Josh Triplett for the pointers.
  * Bump debhelper build-dep and compat to 9.
  * Bump Standards Version to 3.9.4.


 -- Barry deFreese <bdefreese@debian.org>  Thu, 01 Aug 2013 22:43:58 -0400

overgod (1.0-2) unstable; urgency=low

  * Orphan the package, see #711563.
  * Change versioned build dependency liballegro4.2-dev to unversioned
    dependency on liballegro4-dev. (Closes: #710603)

 -- Tobias Hansen <thansen@debian.org>  Sat, 08 Jun 2013 00:10:30 +0200

overgod (1.0-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS with binutils-gold":
    add patch from Ubuntu / Julian Taylor:
    - debian/rules: link with needed libm (LP: #833899)
    (Closes: #555880)
  * Make package binNMUable by using ${source:Version} instead of
    ${Source-Version} (lintian error).

 -- gregor herrmann <gregoa@debian.org>  Tue, 20 Dec 2011 16:38:08 +0100

overgod (1.0-1) unstable; urgency=low

  * Initial Release (Closes: #368275).
  * debian/patches/010_data_path.diff:
    + New patch -- fix data files path to comply with policy.
  * debian/patches/020_config_file.diff:
    + New patch -- use ~/.overgod.cfg as the configuration file path.

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Sat, 20 May 2006 17:11:57 -0500

